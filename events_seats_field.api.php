<?php

/**
 * @file
 * Hooks related to events seats fields.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the room bloc settigns form template path.
 *
 * @param string $templatePath
 *   The old path.
 * @param string $templateId
 *   The template bloc id.
 */
function hook_event_seat_editor_templates_alter(&$templatePath, $templateId) {
  $templatePath = drupal_get_path('module', 'mymodule') . 'templates/forms/';
  switch ($templateId) {
    case 'unnumeredPluginToolbar':
      $templatePath .= 'unnumeredPluginToolbar-settings-form.html.twig';
      break;

    case 'numeredPluginToolbar':
      $templatePath .= 'numeredPluginToolbar-settings-form.html.twig';
      break;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
