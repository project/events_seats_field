<?php

namespace Drupal\events_seats_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Event Seat' formatter.
 *
 * @FieldFormatter(
 *   id = "events_seats_raw",
 *   label = @Translation("Raw Event Seat"),
 *   field_types = {
 *     "events_seats_field_event_seat"
 *   }
 * )
 */
class EventSeatRawFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      // The text value has no text format assigned to it, so the user input
      // should equal the output, including newlines.
      $elements[$delta] = [
        '#type' => 'textarea',
        '#value' => $item->value,
        '#attributes' => [
          'readonly' => 'readonly',
        ],
      ];
    }

    return $elements;
  }

}
