<?php

namespace Drupal\events_seats_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Event Seat' formatter.
 *
 * @FieldFormatter(
 *   id = "events_seats_html",
 *   label = @Translation("Event Seat"),
 *   field_types = {
 *     "events_seats_field_event_seat"
 *   }
 * )
 */
class EventSeatFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'foo' => 'bar',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['foo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Foo'),
      '#default_value' => $this->getSetting('foo'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Foo: @foo', ['@foo' => $this->getSetting('foo')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $element[0] = [
      '#theme' => 'seats_editor',
      '#attached' => [
        'library' => ['events_seats_field/events_seats_field_view'],
      ],
    ];

    return $element;
  }

}
