<?php

namespace Drupal\events_seats_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Component\Uuid\Php;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Defines the 'events_seats_field_event_seat_editor' field widget.
 *
 * @FieldWidget(
 *   id = "events_seats_editor",
 *   label = @Translation("Event Seat Editor"),
 *   field_types = {"events_seats_field_event_seat"},
 * )
 */
class EventSeatEditorWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UUID service.
   *
   * @var \Drupal\Component\Uuid\Php
   */
  protected $uuidService;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHanlder;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Component\Uuid\Php $uuid
   *   Uuid service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager,
    Php $uuid,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
    $this->uuidService = $uuid;
    $this->moduleHanlder = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'debug' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#default_value' => $this->getSetting('debug'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $debug = $this->getSetting('debug');
    $summary[] = $this->t('Debug mode: @debug', ['@debug' => ($debug ? $this->t('Yes') : $this->t('No'))]);

    $mode = $this->getMode();
    $summary[] = $this->t('Working mode: @mode', ['@mode' => $mode]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $buildInfo = $form_state->getUserInput();
    $uuid = '';

    $subItem = $buildInfo;
    $path = array_merge($element['#field_parents'], [
      $items->getName(),
      $delta,
      'uuid_' . implode('_', $element['#field_parents']),
    ]);
    $pathIndex = 0;
    $pathMax = count($path);
    do {
      if (!isset($subItem[$path[$pathIndex]])) {
        $subItem = NULL;
        break;
      }
      $subItem = $subItem[$path[$pathIndex]];
      ++$pathIndex;
    } while ($pathIndex < $pathMax);

    if (!$subItem) {
      $uuid = $this->getUuid();
    }
    else {
      $uuid = $subItem;
    }

    $element['uuid_' . implode('_', $element['#field_parents'])] = [
      '#type' => 'hidden',
      '#value' => $uuid,
    ];
    // Prepare form templates.
    $path = drupal_get_path('module', 'events_seats_field') . "/templates/Forms";
    $forms = dir($path);
    $element['form_templates'] = [
      '#type' => 'content',
      '#prefix' => '<div class="hidden">',
      '#suffix' => '</div>',
    ];
    include_once self::drupalRoot() . '/core/themes/engines/twig/twig.engine';
    while (FALSE !== ($entry = $forms->read())) {
      if ($entry != '.' && $entry != '..') {
        $templateId = str_replace('-settings-form.html.twig', '', $entry);
        $twigPath = $path . '/' . $entry;
        // Allow other modules overwrite the template.
        $this->moduleHanlder->alter('event_seat_editor_templates', $twigPath, $templateId);
        // Render it.
        $output = twig_render_template($twigPath, ['theme_hook_original' => '']);
        $element['form_templates'][$templateId] = [
          '#markup' => urlencode($output),
          '#prefix' => '<div id="esf_twig_' . $templateId . '">',
          '#suffix' => '</div>',
        ];
      }
    }
    // Place editor.
    $roomDefinition = empty($items->getValue()[0]['value']) ? '{"room": {"blocs": []}}' : $items->getValue()[0]['value'];
    $element['value'] = [
      '#type' => 'textarea',
      '#default_value' => $roomDefinition,
      '#attributes' => [
        'autocomplete' => 'off',
        'style' => ['display: none;'],
        'data-esf-id' => 'edit-value-' . $uuid,
        'data-esf-form-id' => $uuid,
        'data-esf-mode' => $this->getMode(),
        'data-esf-debug' => ($this->getSetting('debug') ? '1' : '0'),
      ],
    ];

    $devStyle = '';
    if (!$this->getSetting('debug')) {
      $devStyle = ' class = "hidden"';
    }
    $element['unavailable_seats'] = [
      '#markup' => '[]',
      '#prefix' => '<span data-esf-id="unavailable-seats-' . $uuid . '"' . $devStyle . '>',
      '#suffix' => '</span>',
    ];

    $element['tpl_clone_uuid'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'style' => ['display: none;'],
        'data-esf-id' => 'edit-clone-uuid-' . $uuid,
      ],
    ];

    $element['clone'] = [
      '#type' => 'button',
      '#value' => $this->t('Clone'),
      '#ajax' => [
        'callback' => [get_class($this), 'getTemplateCallback'],
        'disable-refocus' => TRUE,
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#attributes' => [
        'style' => ['display: none;'],
        'data-esf-id' => 'edit-clone-' . $uuid,
        'data-esf-form-id' => $uuid,
        'data-esf-delta' => $delta,
        'data-esf-parents' => array_merge($element['#field_parents'], [$items->getName(), $delta]),
      ],
    ];

    $element['value_editor'] = [
      '#theme' => 'seats_editor',
      '#uuid' => $uuid,
    ];

    $element['#attached']['library'][] = 'events_seats_field/events_seats_field_edit';
    $form['#attached']['drupalSettings']['events_seats_field']['room_template'] = $uuid;

    // Add other templates.
    $templates = $this->entityTypeManager->getStorage('room_template')->loadMultiple();
    $element['#attached']['drupalSettings']['events_seats_field']['templates'] = [];
    foreach ($templates as $template) {
      $element['#attached']['drupalSettings']['events_seats_field']['templates'][$template->label()] = $template->uuid();
    }

    $element['#attached']['library'][] = 'events_seats_field/events_seats_field_edit';

    return $element;
  }

  /**
   * Get a template content.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Callback javascript response.
   */
  public static function getTemplateCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $element = $form_state->getTriggeringElement();
    if (isset($element['#attributes'])) {
      $fieldPath = $element['#attributes']['data-esf-parents'];
      $fieldPathMax = count($fieldPath);
      $fieldPathIndex = 0;
      $subItem = $form_state->getValues();
      do {
        $subItem = $subItem[$fieldPath[$fieldPathIndex]];
        ++$fieldPathIndex;
      } while ($fieldPathIndex < $fieldPathMax);

      $template = \Drupal::entityTypeManager()->getStorage('room_template')->loadByProperties([
        'uuid' => $subItem['tpl_clone_uuid'],
      ]);
      if ($template) {
        $formUuid = $element['#attributes']['data-esf-form-id'];
        $response->addCommand(new InvokeCommand(NULL, 'esfImportTemplate', [$formUuid, current($template)->getValue()]));
      }
    }

    return $response;
  }

  /**
   * Gets the app root.
   *
   * @return string
   *   Gets the app root.
   */
  protected static function drupalRoot() {
    return \Drupal::root();
  }

  /**
   * Get a new UUID.
   *
   * @return string
   *   The UUID.
   */
  protected function getUuid() {
    return $this->uuidService->generate();
  }

  /**
   * Get the widget working mode.
   *
   * @return string
   *   The widget working mode.
   */
  protected function getMode() {
    return 'editor';
  }

}
