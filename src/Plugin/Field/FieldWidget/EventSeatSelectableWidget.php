<?php

namespace Drupal\events_seats_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'events_seats_field_event_seat_editor' field widget.
 *
 * @FieldWidget(
 *   id = "events_seats_selectable",
 *   label = @Translation("Event Seat Selectable"),
 *   field_types = {"events_seats_field_event_seat"},
 * )
 */
class EventSeatSelectableWidget extends EventSeatEditorWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $elements = parent::formElement($items, $delta, $element, $form, $form_state);
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMode() {
    return 'selectable';
  }

}
