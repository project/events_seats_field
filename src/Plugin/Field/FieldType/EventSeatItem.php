<?php

namespace Drupal\events_seats_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;

/**
 * Defines the 'events_seats_field_event_seat' field type.
 *
 * @FieldType(
 *   id = "events_seats_field_event_seat",
 *   label = @Translation("Event Seat"),
 *   category = @Translation("General"),
 *   default_widget = "events_seats_editor",
 *   default_formatter = "events_seats_html"
 * )
 *
 * @see https://www.drupal.org/project/gridstack_field
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class EventSeatItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    try {
      $room = Json::decode($this->get('value')->getValue());
      return (!isset($room['room']['blocs'])) || (count($room['room']['blocs']) == 0);
    }
    catch (InvalidDataTypeException $ex) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Source'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

}
