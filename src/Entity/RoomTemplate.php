<?php

namespace Drupal\events_seats_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Room Template entity.
 *
 * @ConfigEntityType(
 *   id = "room_template",
 *   label = @Translation("Room Template"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\events_seats_field\RoomTemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\events_seats_field\Form\RoomTemplateForm",
 *       "edit" = "Drupal\events_seats_field\Form\RoomTemplateForm",
 *       "delete" = "Drupal\events_seats_field\Form\RoomTemplateDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\events_seats_field\RoomTemplateHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "room_template",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "value" = "room",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/room_template/{room_template}",
 *     "add-form" = "/admin/structure/room_template/add",
 *     "edit-form" = "/admin/structure/room_template/{room_template}/edit",
 *     "delete-form" = "/admin/structure/room_template/{room_template}/delete",
 *     "collection" = "/admin/structure/room_template"
 *   }
 * )
 */
class RoomTemplate extends ConfigEntityBase implements RoomTemplateInterface {

  /**
   * The Room Template ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Room Template label.
   *
   * @var string
   */
  protected $label;

  /**
   * The room template definition.
   *
   * @var string
   */
  protected $value = '{"room": {"blocs": []}}';

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value) {
    $this->value = $value;
  }

}
