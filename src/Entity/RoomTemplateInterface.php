<?php

namespace Drupal\events_seats_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Room Template entities.
 */
interface RoomTemplateInterface extends ConfigEntityInterface {

  /**
   * Get value.
   */
  public function getValue();

  /**
   * Set value.
   *
   * @param string $value
   *   The value.
   */
  public function setValue($value);

}
