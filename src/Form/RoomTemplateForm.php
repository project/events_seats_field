<?php

namespace Drupal\events_seats_field\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class RoomTemplateForm.
 */
class RoomTemplateForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * Constructs a new RoomTemplateForm.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $buildInfo = $form_state->getStorage();
    $userInputs = $form_state->getUserInput();
    $uuid = '';
    if (isset($buildInfo['esf_uuid'])) {
      $uuid = $buildInfo['esf_uuid'];
    }
    elseif (isset($userInputs['esf_form_id'])) {
      $uuid = $userInputs['esf_form_id'];
    }
    else {
      $uuid = $this->entity->uuid();
      $buildInfo['esf_uuid'] = $uuid;
      $form_state->setStorage($buildInfo);
    }

    $form = parent::form($form, $form_state);

    $room_template = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $room_template->label(),
      '#description' => $this->t("Label for the Room Template."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $room_template->id(),
      '#machine_name' => [
        'exists' => '\Drupal\events_seats_field\Entity\RoomTemplate::load',
      ],
      '#disabled' => !$room_template->isNew(),
    ];
    // Prepare form templates.
    $path = drupal_get_path('module', 'events_seats_field') . "/templates/Forms";
    $forms = dir($path);
    $form['form_templates'] = [
      '#type' => 'content',
      '#prefix' => '<div class="hidden">',
      '#suffix' => '</div>',
    ];
    include_once self::drupalRoot() . '/core/themes/engines/twig/twig.engine';
    while (FALSE !== ($entry = $forms->read())) {
      if ($entry != '.' && $entry != '..') {
        $templateId = str_replace('-settings-form.html.twig', '', $entry);
        $twigPath = $path . '/' . $entry;
        // Allow other modules overwrite the template.
        $this->moduleHandler->alter('event_seat_editor_templates', $twigPath, $templateId);
        // Render it.
        $output = twig_render_template($twigPath, ['theme_hook_original' => '']);
        $form['form_templates'][$templateId] = [
          '#markup' => urlencode($output),
          '#prefix' => '<div id="esf_twig_' . $templateId . '">',
          '#suffix' => '</div>',
        ];
      }
    }
    // Place editor.
    $form['esf_form_id'] = [
      '#type' => 'hidden',
      '#value' => $uuid,
    ];

    $form['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Template'),
      '#default_value' => $room_template->getValue(),
      '#attributes' => [
        'style' => ['display: none;'],
        'data-esf-id' => 'edit-value-' . $uuid,
        'data-esf-form-id' => $uuid,
        'data-esf-mode' => 'editor',
        'data-esf-debug' => '0',
      ],
    ];

    $form['tpl_clone_uuid'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'style' => ['display: none;'],
        'data-esf-id' => 'edit-clone-uuid-' . $uuid,
      ],
    ];

    $form['clone'] = [
      '#type' => 'button',
      '#value' => $this->t('Clone'),
      '#ajax' => [
        'callback' => '::getTemplateCallback',
        'disable-refocus' => TRUE,
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#attributes' => [
        'style' => ['display: none;'],
        'data-esf-id' => 'edit-clone-' . $uuid,
        'data-esf-form-id' => $uuid,
        'data-esf-delta' => 0,
        'data-esf-parents' => [],
      ],
    ];

    $form['value_editor'] = [
      '#theme' => 'seats_editor',
      '#uuid' => $uuid,
    ];

    $form['#attached']['library'][] = 'events_seats_field/events_seats_field_edit';
    $form['#attached']['drupalSettings']['events_seats_field']['room_template'] = $uuid;

    // Add other templates.
    $templates = $this->entityTypeManager->getStorage('room_template')->loadMultiple();
    $form['#attached']['drupalSettings']['events_seats_field']['templates'] = [];
    foreach ($templates as $template) {
      if ($template->uuid() != $this->entity->uuid()) {
        $form['#attached']['drupalSettings']['events_seats_field']['templates'][$template->label()] = $template->uuid();
      }
    }

    return $form;
  }

  /**
   * Get a template content.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Callback javascript response.
   */
  public function getTemplateCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $template = $this->entityTypeManager->getStorage('room_template')->loadByProperties([
      'uuid' => $form_state->getValue('tpl_clone_uuid'),
    ]);
    if ($template) {
      $element = $form_state->getTriggeringElement();
      $formUuid = $element['#attributes']['data-esf-form-id'];
      $response->addCommand(new InvokeCommand(NULL, 'esfImportTemplate', [$formUuid, current($template)->getValue()]));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $room_template = $this->entity;
    $status = $room_template->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Room Template.', [
          '%label' => $room_template->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Room Template.', [
          '%label' => $room_template->label(),
        ]));
    }
    $form_state->setRedirectUrl($room_template->toUrl('collection'));
  }

  /**
   * Gets the app root.
   *
   * @return string
   *   Gets the app root.
   */
  protected static function drupalRoot() {
    return \Drupal::root();
  }

}
