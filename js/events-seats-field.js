/**
 * @file
 * Attaches the behaviors for the travel book details page.
 */

(function ($, Drupal, drupalSettings) {

  /**
   * Attaches the fieldUIOverview behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the fieldUIOverview behavior.
   *
   * @see Drupal.fieldUIOverview.attach
   */
  Drupal.behaviors.EventsSeatsField = {
    attach(context, settings) {
      $(function () {
        $('.room-template-editor').each(function (i, editor) {
          if ($(editor).attr('data-started') != 1) {
            $(editor).attr('data-started', '1')
            updateRoomEditor($(editor).attr('data-uuid'));
            // Start the editor.
            $(editor).eventSeatsEditor('init');
          }
        });

        function updateRoomEditor(roomEditorUuid) {
          $("[data-uuid=\"" + roomEditorUuid + "\"] .toolbar").empty();
          var $editor = $("[data-uuid=\"" + roomEditorUuid + "\"]").data('esf_editor');
          if (!$editor) {
            $editor = $("[data-uuid=\"" + roomEditorUuid + "\"]").eventSeatsEditor({
              roomTemplateUuid: roomEditorUuid,
              gridstackSelector: "[data-uuid=\"" + roomEditorUuid + "\"] .grid-stack",
              toolBarSelector: "[data-uuid=\"" + roomEditorUuid + "\"] .toolbar",
              saveToTextarea: $('[data-esf-id="edit-value-' + roomEditorUuid + '"]'),
              unavailableSpan: $('[data-esf-id="unavailable-seats-' + roomEditorUuid + '"]'),
            });
            $("[data-uuid=\"" + roomEditorUuid + "\"]").data('esf_editor', $editor);
          }

          // Add space bloc.
          var pluginSpace = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginSpace.type = 'spacePluginToolbar';
          pluginSpace.getTool = function () {
            var resp = $('<button type="button" class="tool">' + Drupal.t('Space') + '</button>');
            resp.on('click', function () {
              pluginSpace.createBloc({
                x: 0,
                y: 0,
                width: 3,
                height: 5,
                title: Drupal.t('Space'),
                totalSeats: 0,
              });
            });
            return resp;
          }
          pluginSpace.createBloc = function (blocOptions) {
            // Add the bloc.
            pluginSpace.insertBloc(blocOptions);
          }
          pluginSpace.editBloc = function (blocOptions) {
            pluginSpace.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginSpace,
                el: blocOptions.el,
                init: function () {
                  // Init settings form.
                },
                click: function () {
                  // Set configurations.
                },
              }
            }, false);
          }
          pluginSpace.insertBloc = function (blocOptions) {
            pluginSpace.environment.addBloc(pluginSpace, $('<div class="esf-gridstack-item-space"><div class="grid-stack-item-content"></div></div>'), blocOptions);
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginSpace);

          // Add unnumbered bloc.
          var pluginUnnumbered = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginUnnumbered.type = 'unnumeredPluginToolbar';
          pluginUnnumbered.freeSeats = 0;
          pluginUnnumbered.getTool = function () {
            var resp = $('<button type="button" class="tool">' + Drupal.t('Unnumbered') + '</button>');
            resp.on('click', function () {
              pluginUnnumbered.createBloc({
                x: 0,
                y: 0,
                width: 10,
                height: 2,
                title: Drupal.t('Unnumbered'),
                places: 0,
                totalSeats: 0,
              });
            });
            return resp;
          }
          pluginUnnumbered.renderBloc = function (blocOptions) {
            var html = '<div class="title">' + blocOptions.title + '</div>';
            html += '<div class="places">' + Drupal.t('@places places', {'@places': pluginUnnumbered.freeSeats}) + '</div>';
            $(blocOptions.el).find('.grid-stack-item-content').html(html);
          }
          pluginUnnumbered.setUnavailablePlaces = function (node, seats) {
            // Update status.
            pluginUnnumbered.freeSeats -= parseInt(seats);
            pluginUnnumbered.renderBloc(node);
          }
          pluginUnnumbered.showSettings = function (okAction, isnew) {
            if (isnew === undefined) {
              isnew = true;
            }
            // Show settings modal.
            var $formTpl = $(pluginUnnumbered.urldecode($("#esf_twig_unnumeredPluginToolbar").html()));
            pluginUnnumbered.environment.pluginToolbarDisplaySettings(
                    Drupal.t('Unnumberer area settings'),
                    $formTpl,
                    okAction,
                    isnew
                    );
          }
          pluginUnnumbered.createBloc = function (blocOptions) {
            pluginUnnumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginUnnumbered,
                click: function () {
                  // Set configurations.
                  blocOptions.places = $('#esf_upt_places').val();
                  blocOptions.title = $('#esf_upt_title').val();
                  blocOptions.totalSeats = blocOptions.places;
                  // Get custom properties.
                  var properties = $('[data-esf-name]');
                  $.each(properties, function (i, property) {
                    var $prop = $(property);
                    blocOptions[$prop.attr('data-esf-name')] = $prop.val();
                  });
                  // Add the bloc.
                  pluginUnnumbered.insertBloc(blocOptions);
                },
              }
            });
          }
          pluginUnnumbered.editBloc = function (blocOptions) {
            pluginUnnumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginUnnumbered,
                el: blocOptions.el,
                init: function () {
                  // Init settings form.
                  $('#esf_upt_places').val(blocOptions.places);
                  $('#esf_upt_title').val(blocOptions.title);
                  // Set custom properties.
                  var properties = $('[data-esf-name]');
                  $.each(properties, function (i, property) {
                    var $prop = $(property);
                    $prop.val(blocOptions[$prop.attr('data-esf-name')]);
                  });
                },
                click: function () {
                  // Set configurations.
                  blocOptions.places = $('#esf_upt_places').val();
                  blocOptions.title = $('#esf_upt_title').val();
                  blocOptions.totalSeats = blocOptions.places;
                  // Get custom properties.
                  var properties = $('[data-esf-name]');
                  $.each(properties, function (i, property) {
                    var $prop = $(property);
                    blocOptions[$prop.attr('data-esf-name')] = $prop.val();
                  });
                },
              }
            }, false);
          }
          pluginUnnumbered.insertBloc = function (blocOptions) {
            var html = '';
            // The renderBloc function must set the final content.
            // This line only has to init the bloc.
            pluginUnnumbered.freeSeats = blocOptions.places;
            html += '<div class="esf-gridstack-item-unnumbered"><div class="grid-stack-item-content">' + blocOptions.title + '</div>';
            html += '</div>';
            pluginUnnumbered.environment.addBloc(pluginUnnumbered, $(html), blocOptions);
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginUnnumbered);

          // Add numbered bloc.
          var pluginNumbered = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginNumbered.type = 'numeredPluginToolbar';
          pluginNumbered.isNumbered = true;
          pluginNumbered.seatCharts = null;
          pluginNumbered.getTool = function () {
            var resp = $('<button type="button" class="tool">' + Drupal.t('Numbered') + '</button>');
            resp.on('click', function () {
              pluginNumbered.createBloc({
                x: 0,
                y: 0,
                width: 10,
                height: 2,
                title: Drupal.t('Numbered'),
                noResize: true,
                totalSeats: 0,
              });
            });
            return resp;
          }
          pluginNumbered.renderBloc = function (blocOptions) {
            var seatmap = $('<div class="seats" id="seat-map-' + blocOptions.uuid + '" data-bloc-uuid="' + blocOptions.uuid + '"></div>');
            $(blocOptions.el).find('.grid-stack-item-content').empty();
            $(blocOptions.el).find('.grid-stack-item-content').append('<div class="title">' + blocOptions.title + '</div>');
            $(blocOptions.el).find('.grid-stack-item-content').append(seatmap);
            var map = [];
            for (var i = 1; i <= parseInt(blocOptions.rows); i++) {
              map.push('a'.repeat(parseInt(blocOptions.columns)));
            }
            var rowsLabelPositionLeft = !blocOptions.rows_label_position || blocOptions.rows_label_position == 'both' || blocOptions.rows_label_position == 'left';
            var rowsLabelPositionRight = blocOptions.rows_label_position == 'both' || blocOptions.rows_label_position == 'right';
            var columnsNames = [];
            var rowsNames = [];
            // Prepare labels.
            blocOptions.first_column = parseInt(blocOptions.first_column);
            blocOptions.columns = parseInt(blocOptions.columns);
            blocOptions.columns_step = parseInt(blocOptions.columns_step);
            for (var i = blocOptions.first_column; i <= ((blocOptions.columns * blocOptions.columns_step) + blocOptions.first_column); i += blocOptions.columns_step) {
              columnsNames.push(i);
            }
            blocOptions.first_row = parseInt(blocOptions.first_row);
            blocOptions.rows = parseInt(blocOptions.rows);
            for (var i = blocOptions.first_row; i <= (blocOptions.rows + blocOptions.first_row); i++) {
              rowsNames.push(i);
            }
            this.seatCharts = seatmap.seatCharts({
              map: map,
              naming: {
                uuid: blocOptions.uuid,
                top: false,
                left: rowsLabelPositionLeft,
                right: rowsLabelPositionRight,
                columns: columnsNames,
                rows: rowsNames,
                getId  : function (character, row, column) {
                  return this.uuid.replace(/-/g, '') + '_' + row + '_' + column;
                },
              },
              seats: {
                a: {
                  blockUuid: blocOptions.uuid,
                  // Your custom CSS class.
                  classes: 'front-seat',
                }
              },
              click: function () {
                if (pluginNumbered.environment.getMode() !== 'selectable') {
                  return 'available';
                }
                else {
                  if (this.status() == 'available') {
                    var ids = this.settings.id.split('_');
                    // The occupation seat ID.
                    var placeId = blocOptions.uuid + ':' + ids[1] + '_' + ids[2];
                    // @todo: Add to settings json.
                    // Trigger event with the future seat state.
                    $("[data-uuid=\"" + roomEditorUuid + "\"]").trigger("eventSeatEditor:onSeatStatusChanging", {
                      editor_uuid: roomEditorUuid,
                      placed_id: placeId,
                      bloc_uuid: blocOptions.uuid,
                      seat_id: this.settings.id,
                      bloc: ids[0],
                      row: ids[1],
                      column: ids[2],
                      current_state: this.status(),
                      new_state: 'selected',
                    });
                    return 'selected';
                  }
                  else if (this.status() == 'selected') {
                    // Trigger event with the future seat state.
                    var ids = this.settings.id.split('_');
                    // The occupation seat ID.
                    var placeId = blocOptions.uuid + ':' + ids[1] + '_' + ids[2];
                    $("[data-uuid=\"" + roomEditorUuid + "\"]").trigger("eventSeatEditor:onSeatStatusChanging", {
                      editor_uuid: roomEditorUuid,
                      placed_id: placeId,
                      bloc_uuid: blocOptions.uuid,
                      seat_id: this.settings.id,
                      bloc: ids[0],
                      row: ids[1],
                      column: ids[2],
                      current_state: this.status(),
                      new_state: 'available',
                    });
                    // Seat has been vacated.
                    return 'available';
                  }
                  else if (this.status() == 'unavailable') {
                    // Seat has been already booked.
                    return 'unavailable';
                  }
                  else {
                    return this.style();
                  }
                }
              },
              statusChange: function (seat) {
                pluginNumbered.environment.onSelect(seat);
              }
            });
            $(blocOptions.el).data('seat-charts', this.seatCharts);
            var cols = this.seatCharts.settings.columns;
            if (this.seatCharts.settings.left) {
              cols += 1;
            }
            if (this.seatCharts.settings.right) {
              cols += 1;
            }
            var seatWidth = (100 / cols);
            this.seatCharts.find('a').each(function (seatId) {
              var node = this.node();
              $(node).css('width', seatWidth + '%');
            });
            $('#seat-map-' + blocOptions.uuid).find('.seatCharts-space').each(function (i, item) {
              $(item).css('width', seatWidth + '%');
            });
            var rows = this.seatCharts.settings.rows + 1;
            var seatHeight = (100 / rows);
            $('#seat-map-' + blocOptions.uuid).find('.seatCharts-row').each(function (i, item) {
              $(item).css('height', seatHeight + '%');
            });
          }
          pluginNumbered.setUnavailablePlaces = function (node, seats) {
            // Update status.
            var sc = $(node.el).data('seat-charts');
            for (var i = 0; i < seats.length; ++i) {
              sc.get([node.uuid.replace(/-/g, "") + '_' + seats[i]]).status('unavailable');
            }
          }
          pluginNumbered.showSettings = function (okAction, isnew) {
            if (isnew === undefined) {
              isnew = true;
            }
            // Show settings modal.
            var $formTpl = $(pluginNumbered.urldecode($("#esf_twig_numeredPluginToolbar").html()));
            pluginNumbered.environment.pluginToolbarDisplaySettings(
                    Drupal.t('Numberer area settings'),
                    $formTpl,
                    okAction,
                    isnew
                    );
          }
          pluginNumbered.createBloc = function (blocOptions) {
            pluginNumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginNumbered,
                click: function () {
                  // Set configurations.
                  blocOptions.title = $('#esf_npt_title').val();
                  blocOptions.columns = $('#esf_npt_columns').val();
                  blocOptions.rows = $('#esf_npt_rows').val();
                  blocOptions.first_column = $('#esf_npt_first_column').val();
                  blocOptions.first_row = $('#esf_npt_first_row').val();
                  blocOptions.columns_step = $('#esf_npt_columns_step').val();
                  blocOptions.width = (parseInt(blocOptions.columns) + 1);
                  blocOptions.height = parseInt(blocOptions.rows) + 1;
                  blocOptions.totalSeats = blocOptions.rows * blocOptions.columns;
                  blocOptions.rows_label_position = $('#esf_npt_rows_label').val();
                  // Get custom properties.
                  var properties = $('[data-esf-name]');
                  $.each(properties, function (i, property) {
                    var $prop = $(property);
                    blocOptions[$prop.attr('data-esf-name')] = $prop.val();
                  });
                  // Add the bloc.
                  pluginNumbered.insertBloc(blocOptions);
                },
              }
            });
          }
          pluginNumbered.editBloc = function (blocOptions) {
            pluginNumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginNumbered,
                el: blocOptions.el,
                init: function () {
                  // Init settings form.
                  $('#esf_npt_title').val(blocOptions.title);
                  $('#esf_npt_columns').val(blocOptions.columns);
                  $('#esf_npt_rows').val(blocOptions.rows);
                  $('#esf_npt_first_column').val(blocOptions.first_column);
                  $('#esf_npt_first_row').val(blocOptions.first_row);
                  $('#esf_npt_columns_step').val(blocOptions.columns_step);
                  $('#esf_npt_rows_label').val(blocOptions.rows_label_position);
                  // Set custom properties.
                  var properties = $('[data-esf-name]');
                  $.each(properties, function (i, property) {
                    var $prop = $(property);
                    $prop.val(blocOptions[$prop.attr('data-esf-name')]);
                  });
                },
                click: function () {
                  // Set configurations.
                  blocOptions.title = $('#esf_npt_title').val();
                  blocOptions.columns = $('#esf_npt_columns').val();
                  blocOptions.rows = $('#esf_npt_rows').val();
                  blocOptions.first_column = $('#esf_npt_first_column').val();
                  blocOptions.first_row = $('#esf_npt_first_row').val();
                  blocOptions.columns_step = $('#esf_npt_columns_step').val();
                  blocOptions.totalSeats = blocOptions.rows * blocOptions.columns;
                  blocOptions.rows_label_position = $('#esf_npt_rows_label').val();
                  pluginNumbered.environment.getGridstack().resize(blocOptions.el, (parseInt(blocOptions.columns) + 1), parseInt(blocOptions.rows) + 1);
                  // Get custom properties.
                  var properties = $('[data-esf-name]');
                  $.each(properties, function (i, property) {
                    var $prop = $(property);
                    blocOptions[$prop.attr('data-esf-name')] = $prop.val();
                  });
                },
              }
            }, false);
          }
          pluginNumbered.insertBloc = function (blocOptions) {
            var html = '';
            // The renderBloc function must set the final content.
            // This line only has to init the bloc.
            html += '<div class="esf-gridstack-item-unnumbered"><div class="grid-stack-item-content">' + blocOptions.title + '</div>';
            html += '</div>';
            pluginNumbered.environment.addBloc(pluginNumbered, $(html), blocOptions);
          }
          pluginNumbered.getNumberedPlaces = function (node) {
            // This coulb be called bedore seats are started.
            if (!seatCharts) {
              return [];
            }
            var seatCharts = $(node.el).data('seat-charts');
            var ids = seatCharts.seatIds;
            var nIds = [];
            $.each(ids, function (i, id) {
              nIds.push(node.uuid + ":" + id);
            });
            return nIds;
          }
          pluginNumbered.getNumberedSelectedPlaces = function (node) {
            // This could be called before seats are started.
            var seatCharts = $(node.el).data('seat-charts');
            if (!seatCharts) {
              return [];
            }
            var nIds = [];
            seatCharts.find('a.selected').each(function (seatId) {
              nIds.push(this.settings.id);
            });
            // Update any label about this bloc.
            var id = '#numbered_edit_' + node.uuid.replace(/-/g, "");
            $(id + ' .amount').html(nIds.length);
            return nIds;
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginNumbered);

          // Templates.
          var pluginUseTemplate = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginUseTemplate.type = 'useTemplatePluginToolbar';
          pluginUseTemplate.getTool = function () {
            var tplOptions = '';
            $.each(settings.events_seats_field.templates, function (i, item) {
              tplOptions += '<option value="' + item + '">' + i + '</option>';
            });
            var btnId = roomEditorUuid.replace(/-/g, "");
            var resp = $('<label for="slt_template_' + roomEditorUuid + '" class="tool">' + Drupal.t('Template') + '</label>');
            var select = $('<select id="slt_template_' + btnId + '" class="tool">' + tplOptions + '</select>');
            var btn = $('<button type="button" id="btn_template_' + btnId + '" class="tool">' + Drupal.t('Clone') + '</button>');
            btn.on('click', function () {
              var btnClone = $('[data-esf-id="edit-clone-' + roomEditorUuid + '"]');
              $('[data-esf-id="edit-clone-uuid-' + roomEditorUuid + '"]').val(select.val());
              $editor.eventSeatsEditor('reloading');
              pluginUseTemplate.environment.getGridstack().batchUpdate();
              pluginUseTemplate.environment.getGridstack().removeAll();
              btnClone.mousedown();
            });
            $.fn.esfImportTemplate = function (formUuid, template) {
              var $editor = $("[data-uuid=\"" + formUuid + "\"]").data('esf_editor');
              // Replace UUIDs.
              var aux = JSON.parse(template);
              $.each(aux.room.blocs, function (i, bloc) {
                bloc.uuid = $editor.eventSeatsEditor('newGuid');
              });
              template = JSON.stringify(aux, null, '  ');
              // Insert.
              $('[data-esf-id="edit-value-' + formUuid + '"]').val(template);
              $editor.eventSeatsEditor('init');
              pluginUseTemplate.environment.getGridstack().commit();
            };
            resp.push(select[0]);
            resp.push(btn[0]);
            return resp;
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginUseTemplate);

        }

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
