/**
 * @file
 * The jQuery plugin to edit rooms.
 */

(function ($) {

  /**
   * Event seats room editor.
   *
   * @param object options
   *   Settings-
   *
   * @returns array
   *   The event seats room editors.
   *
   * @see https://learn.jquery.com/plugins/
   */
  $.widget('eventSeatsEditor.eventSeatsEditor', {

    // Default options.
    options: {
      roomTemplateUuid: '',
      gridstackSelector: '',
      toolBarSelector: '',
      // Jquery object where to save.
      saveToTextarea: null,
      unavailableSpan: [],
    },

    /**
     * Instance UUID.
     */
    uuid: '',

    /**
     * Working mode.
     */
    mode: 'editor',

    /**
     * Gridstack instance.
     */
    gridstack: null,

    /**
     * Gridstack API.
     */
    gridstackData: null,

    /**
     * Unavailable places by bloc.
     */
    unavailablePlaces: [],

    /**
     * Toolbar jQuery object.
     */
    toolbar: null,

    /**
     * Toolbar plugins instances.
     */
    toolbarPlugins: [],

    /**
     * We are doing the first load.
     */
    _loading: true,

    /**
     * Constructor.
     *
     * @returns object
     *   Plugin instance.
     */
    _create: function () {
      this.uuid = this.options.roomTemplateUuid;
      this.toolbarPlugins = [];
      // Computed default options.
      if (this.options.gridstackSelector === '') {
        this.options.gridstackSelector = '[data-uuid="' + this.options.roomTemplateUuid + '"] .grid-stack';
      }
      if (this.options.toolBarSelector === '') {
        this.options.toolBarSelector = '[data-uuid="' + this.options.roomTemplateUuid + '"] .toolbar';
      }
      // Init resources.
      this.gridstack = $(this.options.gridstackSelector).gridstack({
        disableResize: false,
        column: 40,
        cellHeight: 22,
        verticalMargin: false,
        disableOneColumnMode: true,
      });
      this.gridstackData = this.gridstack.data('gridstack');
      this.toolbar = $(this.options.toolBarSelector);
      if (this.options.saveToTextarea.attr('data-esf-mode')) {
        this.mode = this.options.saveToTextarea.attr('data-esf-mode');
      }
      if (this.options.saveToTextarea.attr('data-esf-debug') === '1') {
        this.options.saveToTextarea.css('display', '');
      }
      try {
        this.unavailablePlaces = JSON.parse(this.options.unavailableSpan.text());
      }
      catch (e) {
        this.unavailablePlaces = [];
      }

      if (this.mode != 'editor') {
        this.toolbar.css('display', 'none');
        this.gridstackData.enableMove(false, true);
        this.gridstackData.enableResize(false, true);
      }
    },
    /**
     * Load saveToTextarea value and init the editor.
     *
     * We can't auto init why we don't know the active plugins.
     */
    init: function () {
      this._loading = true;
      // Load grid.
      var data = JSON.parse(this.options.saveToTextarea.val());
      var me = this;
      var maxColumns = [];
      this.getGridstack().batchUpdate();
      $.each(data.room.blocs, function (i, item) {
        var handler = me.pluginToolbarGet(item.type);
        if (handler) {
          // Create the block.
          if (!maxColumns[item.x]) {
            maxColumns[item.x] = 0;
          }
          var nVal = item.x + item.width;
          if (nVal > maxColumns[item.x]) {
            maxColumns[item.x] = nVal;
          }
          handler.insertBloc(item);
          handler.renderBloc(item);
          if (me.mode === 'selectable') {
            if (me.unavailablePlaces[item.uuid]) {
              handler.setUnavailablePlaces(item, me.unavailablePlaces[item.uuid]);
            }
          }
        }
      });
      this.getGridstack().commit();
      if (me.mode === 'selectable') {
        var max = 0;
        $.each(maxColumns, function (i, val) {
          if (val && val > max) {
            max = val;
          }
        });
        // @todo: Change columns to use all available space.
        this.getGridstack().setColumn(40);
      }
      // End load.
      this._loading = false;
      // Changed event handler.
      var me = this;
      $(this.options.gridstackSelector).on('change', function (event, items) {
        me.changed(event, items);
      });
    },
    // _setOptions is called with a hash of all options that are changing
    // always refresh when changing options.
    _setOptions: function () {
      // _super and _superApply handle keeping the right this-context.
      this._superApply(arguments);
    },
    // _setOption is called for each individual option that is changing.
    _setOption: function (key, value) {
      this._super(key, value);
    },
    /**
     * Stop changed event handler.
     *
     * @returns {undefined}
     */
    reloading: function () {
      this._loading = true;
    },
    /**
     * Process a seat status change.
     *
     * @param object seat
     *   The selected seat.
     *
     * @returns {undefined}
     */
    onSelect: function (seat) {
      if (this.mode === 'selectable') {
        var selecteds = $(this.options.gridstackSelector + ' > .grid-stack-item:visible').map(function (i, el) {
          el = $(el);
          var node = el.data('_gridstack_node');
          var $resp = [];
          var selecteds = node.esfPlugin.getNumberedSelectedPlaces(node);
          $.each(selecteds, function (i, item) {
            $resp.push(item);
          });
          return $resp;
        }).toArray();
        var json = JSON.parse(this.options.saveToTextarea.val());
        json.room.selected = selecteds;
        this.options.saveToTextarea.val(JSON.stringify(json, null, '  '));
      }
    },
    /**
     * Update exportable data.
     *
     * @param object event
     *   Event object.
     * @param object items
     *   Items.
     *
     * @returns {undefined}
     */
    changed: function (event, items) {
      if (this._loading) {
        // While loading we don't want destroy de source data.
        return;
      }
      if (!this.options.saveToTextarea) {
        return;
      }
      var exportable = {
        room: {
          totalSeats: 0,
          selected: {},
          allowedPlaces: {
            numbered: [],
            unnumbered: 0,
          },
          blocs: {},
        },
      };
      var serializedData = $(this.options.gridstackSelector + ' > .grid-stack-item:visible').map(function (i, el) {
        el = $(el);
        var node = el.data('_gridstack_node');
        var $resp = {};
        for (const prop in node) {
          if (typeof node[prop] !== 'object' && !(prop.startsWith('_') || prop.startsWith('lastTried'))) {
            $resp[prop] = node[prop];
            if (prop === 'totalSeats') {
              exportable.room.totalSeats += parseInt(node[prop]);
              if (node.esfPlugin.isNumbered) {
                var myPlaces = node.esfPlugin.getNumberedPlaces(node);
                $.each(myPlaces, function (i, id) {
                  exportable.room.allowedPlaces.numbered.push(id);
                });
              }
              else {
                exportable.room.allowedPlaces.unnumbered += parseInt(node[prop]);
              }
            }
          }
        }
        if (node["esfPlugin"]) {
          // Render the block and allow customize data.
          node["esfPlugin"].renderBloc(node);
          $resp = node["esfPlugin"].setExport($resp);
        }
        return $resp;
      }).toArray();
      exportable.room.blocs = serializedData;
      this.options.saveToTextarea.val(JSON.stringify(exportable, null, '  '));
    },
    /**
     * Add a new bloc.
     *
     * @param object caller
     *   The plugin handler.
     * @param jquery element
     *   The content element.
     * @param object options
     *   Options.
     *
     * @returns {undefined}
     */
    addBloc: function (caller, element, options) {
      if (this.mode == 'editor') {
        var gear = $('<div class="ui-icon ui-icon-gear" style="display:none;"></div>');
        element.on('mouseenter', function (e) {
          gear.css('display', '');
        });
        element.on('mouseleave', function (e) {
          gear.css('display', 'none');
        });
        element.append(gear);
      }
      var newBloc = this.getGridstack().addWidget(element, options);
      if (options.noResize === true) {
        this.getGridstack().resizable(element, false);
      }
      var data = newBloc.data('_gridstack_node');
      data["esfPlugin"] = caller;
      data["type"] = caller.type;
      for (const prop in options) {
        data[prop] = options[prop];
      }
      if (!data["uuid"]) {
        data["uuid"] = this.newGuid();
      }
      newBloc.data('_gridstack_node', data);
      if (this.mode == 'editor') {
        gear.on('click', function (e) {
          caller.editBloc(newBloc.data('_gridstack_node'));
        });
      }
      if (!options.el) {
        options.el = element;
      }
      this.changed();
    },

    /**
     * The gridstack instance.
     *
     * @returns object
     *   Gridstack.
     */
    getGridstack: function () {
      return this.gridstackData;
    },
    /**
     * Get the widget Uuid.
     *
     * @returns string
     *   The widget Uuid.
     */
    getUuid: function () {
      return this.uuid;
    },
    /**
     * Get the working mode.
     *
     * @returns string
     *   The current working mode. Values editor, selectable, viewer.
     */
    getMode: function () {
      return this.mode;
    },
    /**
     * Get a default toolbar plugin handler.
     *
     * @return object
     *   The default handler.
     */
    pluginToolbarCreateDefaultHandler: function () {
      return {
        /**
         * Exportable properties.
         */
        type: 'nullPluginToolbar',
        /**
         * Internal.
         */
        pluginValidation: 'Event Seats Editor Toolbar Plugin',
        /**
         * The editor.
         */
        environment: this,
        /**
         * If this bloc count how a numbered bloc.
         */
        isNumbered: false,
        /**
         * Allow customize exportable data.
         *
         * @param object collection
         *   The exportable data.
         *
         * @returns {unresolved}
         *   Fixed exportable data.
         */
        setExport: function (collection) {
          return collection;
        },
        /**
         * Get de tollbar widget with complete functionality.
         *
         * @returns {jQuery|$|@pro;window@pro;$|Window.$|object}
         */
        getTool: function () {
          var resp = $('<button type="button">Example</button>');
          resp.on('click', function () {
            console.log('Example button clicked.');
          });
          return resp;
        },
        /**
         * Get a list of numbered places.
         *
         * @param object node
         *   The gridstack meta data info.
         *
         * @returns {Array}
         */
        getNumberedPlaces: function (node) {
          return [];
        },
        /**
         * Get a list of numbered selected places.
         *
         * @param object node
         *   The gridstack meta data info.
         *
         * @returns {Array}
         */
        getNumberedSelectedPlaces: function (node) {
          return [];
        },
        /**
         * Update unavailable seat status.
         *
         * @param object node
         *   The gridstack meta data info.
         * @param mixed seats
         *   Unavailable seats. Each plugin define her format.
         *
         * @returns {undefined}
         */
        setUnavailablePlaces: function (node, seats) {},
        /**
         * Create a new bloc of this type.
         *
         * @param object options
         *   Options object.
         *
         * @returns {undefined}
         *   Nothing.
         */
        createBloc: function (blocOptions) {},
        /**
         * Edit a bloc of this type.
         *
         * @param object options
         *   Options object.
         *
         * @returns {undefined}
         *   Nothing.
         */
        editBloc: function (blocOptions) {},
        /**
         * Do the final bloc insert.
         *
         * @param object blocOptions
         *   Options.
         *
         * @returns {undefined}
         */
        insertBloc: function (blocOptions) {},
        /**
         * Render the bloc of this type.
         *
         * @param object options
         *   Options object.
         *
         * @returns {undefined}
         *   Nothing.
         */
        renderBloc: function (blocOptions) {},
        /**
         * Show settings dialog.
         *
         * @param object okAction
         *   The action.
         *
         * @returns {undefined}
         */
        showSettings: function (okAction, isnew) {
          if (isnew === undefined) {
            isnew = true;
          }
          // Show settings modal.
          var $formTpl = $('<div>&nbsp;</div>');
          this.environment.pluginToolbarDisplaySettings(
            Drupal.t('Bloc'),
            $formTpl,
            okAction,
            isnew
          );
        },
        /**
         * Decode a URL encoded string.
         *
         * @param string str
         *   URL encoded string.
         *
         * @returns string
         *   Decoded string.
         *
         * @see https://stackoverflow.com/a/4458580/6385708
         */
        urldecode: function (str) {
          return decodeURIComponent((str + '').replace(/\+/g, '%20'));
        },
      };
    },

    /**
     * Add a new toolbar plugin.
     *
     * @param object handler
     *   The toolbar plugin handler.
     */
    pluginToolbarAdd: function (plugin) {
      // Validate toolbar plugin.
      if (plugin.pluginValidation !== 'Event Seats Editor Toolbar Plugin') {
        throw "eventSeatsEditor::pluginToolbarAdd requires a valid toolbar pluging handler."
      }
      if (!this.pluginToolbarGet(plugin.type)) {
        // Add the new plugin.
        this.toolbarPlugins.push(plugin);
        if (this.mode == 'editor') {
          this.toolbar.append(plugin.getTool());
        }
      }
    },

    /**
     * Display the settings form.
     *
     * @param string title
     *   Dialog title.
     * @param object body
     *   jQuery form element.
     * @param object applyButton
     *   Dialog button object.
     * @param bool isnew
     *   TRUE if is a new bloc. Else display the remove button.
     *
     * @returns {undefined}
     */
    pluginToolbarDisplaySettings: function (title, body, applyButton, isnew) {
      if (isnew === undefined) {
        isnew = true;
      }
      var widget = this;
      applyButton.click = function () {
        // Update settigns.
        applyButton.control.click();
        // Update field value.
        widget.changed();
        confirmationDialog.close();
      }
      var options = {
        title: title,
        width: "50%",
        height: "50%",
        dialogClass: '',
        resizable: false,
        closeOnEscape: true,
        buttons: [
          applyButton,
          {
            text: Drupal.t('Cancel'),
            class: 'button',
            click() {
              confirmationDialog.close();
            },
          },
        ],
        create: function () {
          $(this).parent().find('.ui-dialog-titlebar-close').remove();
          if (applyButton.control.init) {
            applyButton.control.init();
          }
        },
        beforeClose: false,
        close: function (event) {
          $(event.target).remove();
        }
      };
      if (isnew !== true) {
        options.buttons.push({
          text: Drupal.t('Remove'),
          class: 'button button--danger',
          click() {
            // Remove the item.
            widget.getGridstack().removeWidget(applyButton.control.el);
            confirmationDialog.close();
          },
        });
      }
      var confirmationDialog = Drupal.dialog(body, options);
      confirmationDialog.showModal();
    },
    /**
     * Get the plugin handler by type.
     *
     * @param string type
     *   The required type.
     *
     * @returns object
     *   The plugin handler or null.
     */
    pluginToolbarGet: function (type) {
      for (var index in this.toolbarPlugins) {
        if (this.toolbarPlugins[index].type === type) {
          return this.toolbarPlugins[index];
        }
      }
      return null;
    },
    /**
     * Create a random Guid.
     *
     * @return {String} a random guid value.
     */
    newGuid: function () {
      return 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
        function (c) {
          var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    },

  });

}(jQuery));
