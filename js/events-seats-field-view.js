/**
 * @file
 * Attaches the behaviors for the travel book details page.
 */

(function ($, Drupal, drupalSettings) {

  /**
   * Attaches the fieldUIOverview behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the fieldUIOverview behavior.
   *
   * @see Drupal.fieldUIOverview.attach
   */
  Drupal.behaviors.EventsSeatsField = {
    attach(context, settings) {
      $(function () {
        $('.room-template-editor').each(function (i, editor) {
          console.log($(editor).attr('data-uuid'));
        });

        function updateRoomEditor(roomEditorUuid) {
          $("[data-uuid=\"" + roomEditorUuid + "\"] .toolbar").empty();
          var $editor = $("[data-uuid=\"" + roomEditorUuid + "\"]").data('esf_editor');
          if (!$editor) {
            $editor = $("[data-uuid=\"" + roomEditorUuid + "\"]").eventSeatsEditor({
              roomTemplateUuid: roomEditorUuid,
              gridstackSelector: "[data-uuid=\"" + roomEditorUuid + "\"] .grid-stack",
              toolBarSelector: "[data-uuid=\"" + roomEditorUuid + "\"] .toolbar",
              saveToTextarea: $('[data-esf-id="edit-value-' + roomEditorUuid + '"]'),
            });
            $("[data-uuid=\"" + roomEditorUuid + "\"]").data('esf_editor', $editor);
          }

          // Add space bloc.
          var pluginSpace = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginSpace.type = 'spacePluginToolbar';
          pluginSpace.getTool = function () {
            var resp = $('<button type="button" class="tool">' + Drupal.t('Space') + '</button>');
            resp.on('click', function () {
              pluginSpace.createBloc({
                x: 0,
                y: 0,
                width: 3,
                height: 5,
                title: Drupal.t('Space'),
                totalSeats: 0,
              });
            });
            return resp;
          }
          pluginSpace.createBloc = function (blocOptions) {
            // Add the bloc.
            pluginSpace.insertBloc(blocOptions);
          }
          pluginSpace.editBloc = function (blocOptions) {
            pluginSpace.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginSpace,
                el: blocOptions.el,
                init: function () {
                  // Init settings form.
                },
                click: function () {
                  // Set configurations.
                },
              }
            }, false);
          }
          pluginSpace.insertBloc = function (blocOptions) {
            pluginSpace.environment.addBloc(pluginSpace, $('<div class="esf-gridstack-item-space"><div class="grid-stack-item-content"></div></div>'), blocOptions);
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginSpace);

          // Add unnumbered bloc.
          var pluginUnnumbered = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginUnnumbered.type = 'unnumeredPluginToolbar';
          pluginUnnumbered.getTool = function () {
            var resp = $('<button type="button" class="tool">' + Drupal.t('Unnumbered') + '</button>');
            resp.on('click', function () {
              pluginUnnumbered.createBloc({
                x: 0,
                y: 0,
                width: 10,
                height: 2,
                title: Drupal.t('Unnumbered'),
                places: 0,
                totalSeats: 0,
              });
            });
            return resp;
          }
          pluginUnnumbered.renderBloc = function (blocOptions) {
            var html = '<div class="title">' + blocOptions.title + '</div>';
            html += '<div class="places">' + Drupal.t('@places places', {'@places': blocOptions.places}) + '</div>';
            $(blocOptions.el).find('.grid-stack-item-content').html(html);
          }
          pluginUnnumbered.showSettings = function (okAction, isnew) {
            if (isnew === undefined) {
              isnew = true;
            }
            // Show settings modal.
            var $formTpl = $(pluginUnnumbered.urldecode($("#esf_twig_unnumeredPluginToolbar").html()));
            pluginUnnumbered.environment.pluginToolbarDisplaySettings(
              Drupal.t('Unnumberer area settings'),
              $formTpl,
              okAction,
              isnew
            );
          }
          pluginUnnumbered.createBloc = function (blocOptions) {
            pluginUnnumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginUnnumbered,
                click: function () {
                  // Set configurations.
                  blocOptions.places = $('#esf_upt_places').val();
                  blocOptions.title = $('#esf_upt_title').val();
                  blocOptions.totalSeats = blocOptions.places;
                  // Add the bloc.
                  pluginUnnumbered.insertBloc(blocOptions);
                },
              }
            });
          }
          pluginUnnumbered.editBloc = function (blocOptions) {
            pluginUnnumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginUnnumbered,
                el: blocOptions.el,
                init: function () {
                  // Init settings form.
                  $('#esf_upt_places').val(blocOptions.places);
                  $('#esf_upt_title').val(blocOptions.title);
                },
                click: function () {
                  // Set configurations.
                  blocOptions.places = $('#esf_upt_places').val();
                  blocOptions.title = $('#esf_upt_title').val();
                  blocOptions.totalSeats = blocOptions.places;
                },
              }
            }, false);
          }
          pluginUnnumbered.insertBloc = function (blocOptions) {
            var html = '';
            // The renderBloc function must set the final content.
            // This line only has to init the bloc.
            html += '<div class="esf-gridstack-item-unnumbered"><div class="grid-stack-item-content">' + blocOptions.title + '</div>';
            html += '</div>';
            pluginUnnumbered.environment.addBloc(pluginUnnumbered, $(html), blocOptions);
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginUnnumbered);

          // Add numbered bloc.
          var pluginNumbered = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginNumbered.type = 'numeredPluginToolbar';
          pluginNumbered.isNumbered = true;
          pluginNumbered.seatCharts = null;
          pluginNumbered.getTool = function () {
            var resp = $('<button type="button" class="tool">' + Drupal.t('Numbered') + '</button>');
            resp.on('click', function () {
              pluginNumbered.createBloc({
                x: 0,
                y: 0,
                width: 10,
                height: 2,
                title: Drupal.t('Numbered'),
                noResize: true,
                totalSeats: 0,
              });
            });
            return resp;
          }
          pluginNumbered.renderBloc = function (blocOptions) {
            var seatmap = $('<div class="seats" id="seat-map-' + blocOptions.uuid + '"></div>');
            $(blocOptions.el).find('.grid-stack-item-content').empty();
            $(blocOptions.el).find('.grid-stack-item-content').append('<div class="title">' + blocOptions.title + '</div>');
            $(blocOptions.el).find('.grid-stack-item-content').append(seatmap);
            var map = [];
            for (var i = 1; i <= parseInt(blocOptions.rows); i++) {
              map.push('a'.repeat(parseInt(blocOptions.columns)));
            }
            var rowsLabelPositionLeft = !blocOptions.rows_label_position || blocOptions.rows_label_position == 'both' || blocOptions.rows_label_position == 'left';
            var rowsLabelPositionRight = blocOptions.rows_label_position == 'both' || blocOptions.rows_label_position == 'right';
            var columnsNames = [];
            var rowsNames = [];
            // Prepare labels.
            blocOptions.first_column = parseInt(blocOptions.first_column);
            blocOptions.columns = parseInt(blocOptions.columns);
            blocOptions.columns_step = parseInt(blocOptions.columns_step);
            for (var i = blocOptions.first_column; i <= ((blocOptions.columns * blocOptions.columns_step) + blocOptions.first_column); i += blocOptions.columns_step) {
              columnsNames.push(i);
            }
            blocOptions.first_row = parseInt(blocOptions.first_row);
            blocOptions.rows = parseInt(blocOptions.rows);
            for (var i = blocOptions.first_row; i <= (blocOptions.rows + blocOptions.first_row); i++) {
              rowsNames.push(i);
            }
            this.seatCharts = seatmap.seatCharts({
              map: map,
              naming: {
                top: false,
                left: rowsLabelPositionLeft,
                right: rowsLabelPositionRight,
                columns: columnsNames,
                rows: rowsNames
              },
              seats: {
                a: {
                  // Your custom CSS class.
                  classes: 'front-seat',
                }
              },
              click: function () {
                return 'available';
              }
            });
            $(blocOptions.el).data('seat-charts', this.seatCharts);
            var cols = this.seatCharts.settings.columns;
            if (this.seatCharts.settings.left) {
              cols += 1;
            }
            if (this.seatCharts.settings.right) {
              cols += 1;
            }
            var seatWidth = (100 / cols);
            this.seatCharts.find('a').each(function (seatId) {
              var node = this.node();
              $(node).css('width', seatWidth + '%');
           });
           $('#seat-map-' + blocOptions.uuid).find('.seatCharts-space').each(function (i, item) {
             $(item).css('width', seatWidth + '%');
           });
           var rows = this.seatCharts.settings.rows + 1;
           var seatHeight = (100 / rows);
           $('#seat-map-' + blocOptions.uuid).find('.seatCharts-row').each(function (i, item) {
             $(item).css('height', seatHeight + '%');
           });
          }
          pluginNumbered.showSettings = function (okAction, isnew) {
            if (isnew === undefined) {
              isnew = true;
            }
            // Show settings modal.
            var $formTpl = $(pluginNumbered.urldecode($("#esf_twig_numeredPluginToolbar").html()));
            pluginNumbered.environment.pluginToolbarDisplaySettings(
              Drupal.t('Numberer area settings'),
              $formTpl,
              okAction,
              isnew
            );
          }
          pluginNumbered.createBloc = function (blocOptions) {
            pluginNumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginNumbered,
                click: function () {
                  // Set configurations.
                  blocOptions.title = $('#esf_npt_title').val();
                  blocOptions.columns = $('#esf_npt_columns').val();
                  blocOptions.rows = $('#esf_npt_rows').val();
                  blocOptions.first_column = $('#esf_npt_first_column').val();
                  blocOptions.first_row = $('#esf_npt_first_row').val();
                  blocOptions.columns_step = $('#esf_npt_columns_step').val();
                  blocOptions.width = (parseInt(blocOptions.columns) + 1);
                  blocOptions.height = parseInt(blocOptions.rows) + 1;
                  blocOptions.totalSeats = blocOptions.rows * blocOptions.columns;
                  blocOptions.rows_label_position = $('#esf_npt_rows_label').val();
                  // Add the bloc.
                  pluginNumbered.insertBloc(blocOptions);
                },
              }
            });
          }
          pluginNumbered.editBloc = function (blocOptions) {
            pluginNumbered.showSettings({
              text: Drupal.t('Ok'),
              class: 'button button--primary',
              control: {
                plugin: pluginNumbered,
                el: blocOptions.el,
                init: function () {
                  // Init settings form.
                  $('#esf_npt_title').val(blocOptions.title);
                  $('#esf_npt_columns').val(blocOptions.columns);
                  $('#esf_npt_rows').val(blocOptions.rows);
                  $('#esf_npt_first_column').val(blocOptions.first_column);
                  $('#esf_npt_first_row').val(blocOptions.first_row);
                  $('#esf_npt_columns_step').val(blocOptions.columns_step);
                  $('#esf_npt_rows_label').val(blocOptions.rows_label_position);
                },
                click: function () {
                  // Set configurations.
                  blocOptions.title = $('#esf_npt_title').val();
                  blocOptions.columns = $('#esf_npt_columns').val();
                  blocOptions.rows = $('#esf_npt_rows').val();
                  blocOptions.first_column = $('#esf_npt_first_column').val();
                  blocOptions.first_row = $('#esf_npt_first_row').val();
                  blocOptions.columns_step = $('#esf_npt_columns_step').val();
                  blocOptions.totalSeats = blocOptions.rows * blocOptions.columns;
                  blocOptions.rows_label_position = $('#esf_npt_rows_label').val();
                  pluginNumbered.environment.getGridstack().resize(blocOptions.el, (parseInt(blocOptions.columns) + 1), parseInt(blocOptions.rows) + 1);
                },
              }
            }, false);
          }
          pluginNumbered.insertBloc = function (blocOptions) {
            var html = '';
            // The renderBloc function must set the final content.
            // This line only has to init the bloc.
            html += '<div class="esf-gridstack-item-unnumbered"><div class="grid-stack-item-content">' + blocOptions.title + '</div>';
            html += '</div>';
            pluginNumbered.environment.addBloc(pluginNumbered, $(html), blocOptions);
          }
          pluginNumbered.getNumberedPlaces = function (node) {
            var ids = this.seatCharts.seatIds;
            var nIds = [];
            $.each(ids, function (i, id) {
              nIds.push(node.uuid + ":" + id);
            });
            return nIds;
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginNumbered);

          // Templates.
          var pluginUseTemplate = $editor.eventSeatsEditor('pluginToolbarCreateDefaultHandler');
          pluginUseTemplate.type = 'useTemplatePluginToolbar';
          pluginUseTemplate.getTool = function () {
            var tplOptions = '';
            $.each(settings.events_seats_field.templates, function (i, item) {
              tplOptions += '<option value="' + item + '">' + i + '</option>';
            });
            var btnId = roomEditorUuid.replace(/-/g, "");
            var resp = $('<label for="slt_template_' + roomEditorUuid + '" class="tool">' + Drupal.t('Template') + '</label>');
            var select = $('<select id="slt_template_' + btnId + '" class="tool">' + tplOptions + '</select>');
            var btn = $('<button type="button" id="btn_template_' + btnId + '" class="tool">' + Drupal.t('Clone') + '</button>');
            btn.on('click', function () {
              var btnClone = $('[data-esf-id="edit-clone-' + roomEditorUuid + '"]');
              $('[data-esf-id="edit-clone-uuid-' + roomEditorUuid + '"]').val(select.val());
              $editor.eventSeatsEditor('reloading');
              pluginUseTemplate.environment.getGridstack().batchUpdate();
              pluginUseTemplate.environment.getGridstack().removeAll();
              btnClone.mousedown();
            });
            $.fn.esfImportTemplate = function (formUuid, template) {
              // Replace UUIDs.
              var aux = JSON.parse(template);
              $.each(aux.room.blocs, function (i, bloc) {
                bloc.uuid = $editor.eventSeatsEditor('newGuid');
              });
              template = JSON.stringify(aux, null, '  ');
              // Insert.
              $('[data-esf-id="edit-value-' + formUuid + '"]').val(template);
              pluginUseTemplate.environment.getGridstack().commit();
            };
            resp.push(select[0]);
            resp.push(btn[0]);
            return resp;
          }
          $editor.eventSeatsEditor('pluginToolbarAdd', pluginUseTemplate);
          // Start the editor.
          $editor.eventSeatsEditor('init');
        }

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
