/**
 * @file
 * Attaches the behaviors for the travel book details page.
 */

(function ($, Drupal, drupalSettings) {

  /**
   * Attaches the fieldUIOverview behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the fieldUIOverview behavior.
   *
   * @see Drupal.fieldUIOverview.attach
   */
  Drupal.behaviors.EventsSeatsFieldDemo = {
    attach(context, settings) {
      $(function () {
        $('.room-template-editor').each(function (i, editor) {
          // Add seat status event handler.
          $("[data-uuid=\"" + $(editor).attr('data-uuid') + "\"]").once().on("eventSeatEditor:onSeatStatusChanging", function (event, data) {
            console.log('data', data);
          });
        });

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
